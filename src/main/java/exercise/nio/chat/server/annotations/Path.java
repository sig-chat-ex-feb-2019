package exercise.nio.chat.server.annotations;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * Annotation indicating that a Type corresponds to and may capture elements
 * from a URI path. E.g. the path /foo should correspond only to path /foo.
 * The path /foo/{bar}/baz would correspond to as many paths as exist single
 * path elements between foo and baz.
 */
@Documented
@Retention(RUNTIME)
@Target(TYPE)
public @interface Path {
    /**
     * Priority to check for a match between routes that are keyed on the same
     * base path. A higher number means a route will be checked first.
     */
    final class Priority {
        private Priority() { }
        public static final int HIGH = 10;
        public static final int DEFAULT = 1;
        public static final int LOW = -10;
    }
    String value();
}
