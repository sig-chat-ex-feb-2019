package exercise.nio.chat.server.method;

import exercise.nio.chat.server.Activity;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;

import com.google.common.net.HttpHeaders;

import exercise.nio.chat.server.annotations.Path;
import exercise.nio.chat.server.http.CommonResponses;
import exercise.nio.chat.server.http.HttpConstants;
import exercise.nio.chat.server.http.HttpRequest;
import exercise.nio.chat.server.http.HttpResponse;

/**
 * A simple echo server
 */
@Path("/echo")
public class Echo implements Activity {
    private static final Charset UTF_8 = Charset.forName("UTF-8");

    @Override
    public ByteBuffer[] invoke(HttpRequest request, Object... args) {
        switch (request.getMethod()) {
        case "GET":
        case "HEAD":
        case "POST":
        case "PUT":
            return doEcho(request);
        }
        return new ByteBuffer[] {ByteBuffer.wrap(CommonResponses.BAD_REQUEST)};
    }

    @Override
    public int getQueueingNiceness() {
        return Activity.NICENESS_HIGH;
    }

    @Override
    public boolean shouldRetry() {
        return false;
    }

    /**
     * Echo a request
     * @param request the HTTP request to echo
     * @return the echoed request as a response
     */
    protected ByteBuffer[] doEcho(HttpRequest request) {
        final byte[] requestHeaders = request.getHeaderString().getBytes(UTF_8);
        final byte[] body = null == request.getMessageBody() ? new byte[0] :
            request.getMessageBody();
        final HttpResponse response =
                new HttpResponse.Builder(200, "OK")
                .header(HttpHeaders.CONTENT_TYPE, HttpConstants.APPLICATION_HTTP)
                .header(HttpHeaders.CONTENT_LENGTH,
                        Integer.toString(requestHeaders.length + body.length))
                .build();
        return new ByteBuffer[] {
                ByteBuffer.wrap(HttpResponse.serialize(response)),
                ByteBuffer.wrap(requestHeaders), ByteBuffer.wrap(body)};
    }
}
