package exercise.nio.chat.server.method;

import javax.annotation.Priority;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.net.HttpHeaders;

import exercise.nio.chat.server.annotations.Path;
import exercise.nio.chat.server.data.Chat;
import exercise.nio.chat.server.data.Message;
import exercise.nio.chat.server.data.storage.MapDBStorage;
import exercise.nio.chat.server.http.CommonResponses;
import exercise.nio.chat.server.http.HttpConstants;
import exercise.nio.chat.server.http.HttpRequest;
import exercise.nio.chat.server.http.HttpResponse;
import exercise.nio.chat.server.Activity;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Collections;
import java.util.NavigableSet;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * An Activity for adding and retrieving chat messages.
 * The priority for the routing path is set to high as message
 * requests are more frequent than chat requests.
 */
@Priority(Path.Priority.HIGH)
@Path("/chats/{chatId}/messages")
public class Messages implements Activity {
    private static final ObjectMapper MAPPER = new ObjectMapper();
    private final AtomicBoolean retry = new AtomicBoolean(false);

    @Override
    public ByteBuffer[] invoke(HttpRequest request, Object... args)
            throws IOException {
        switch (request.getMethod()) {
        case "POST":
            return doAddMessage(request, (String) args[0]);

        case "GET":
            return doGetMessages(request, (String) args[0]);
        }
        return new ByteBuffer[] {ByteBuffer.wrap(CommonResponses.BAD_REQUEST)};
    }

    @Override
    public int getQueueingNiceness() {
        return Activity.NICENESS_DEFAULT;
    }

    @Override
    public boolean shouldRetry() {
        return retry.get();
    }

    /**
     * Add a message to a chat
     * The chat must exist.
     * The message source and destination users must be chat participants.
     * @param request the HTTP request
     * @param chatId the chatId as extracted from the path
     * @return an HTTP response
     */
    protected ByteBuffer[] doAddMessage(HttpRequest request, String chatId) {
        final long id;
        try {
            id = Long.parseLong(chatId);
        } catch(NumberFormatException e) {
            return new ByteBuffer[]
                    {ByteBuffer.wrap(CommonResponses.BAD_REQUEST)};
        }
        final MapDBStorage db = MapDBStorage.getInstance();
        final Chat chat = db.getChat(id);
        if (null == chat) {
            retry.set(true);
            return new ByteBuffer[]
                    {ByteBuffer.wrap(CommonResponses.NOT_FOUND)};
        }
        retry.set(false);
        final Message message;
        try {
             message = MAPPER.readValue(request.getMessageBody(), Message.class);
        } catch (IOException e) {
            return new ByteBuffer[]
                    {ByteBuffer.wrap(CommonResponses.BAD_REQUEST)};
        }
        if (!chat.getParticipantIds().contains(message.getSourceUserId()) ||
                !chat.getParticipantIds().contains(message.getDestinationUserId())) {
            return new ByteBuffer[]
                    {ByteBuffer.wrap(CommonResponses.FORBIDDEN)};
        }
        MapDBStorage.getInstance().persist(id, message);
        return new ByteBuffer[] {ByteBuffer.wrap(CommonResponses.OK)};
    }

    /**
     * Retrieve chat messages
     * @param request the HttpRequest
     * @param chatId the chatId as extracted from the path
     * @return an HTTP response
     * @throws IOException if there is an exception serializing the response
     */
    protected ByteBuffer[] doGetMessages(HttpRequest request, String chatId)
            throws IOException {
        final long id;
        try {
            id = Long.parseLong(chatId);
        } catch(NumberFormatException e) {
            return new ByteBuffer[]
                    {ByteBuffer.wrap(CommonResponses.BAD_REQUEST)};
        }
        if (null == MapDBStorage.getInstance().getChat(id)) {
            return new ByteBuffer[]
                    {ByteBuffer.wrap(CommonResponses.NOT_FOUND)};
        }
        final NavigableSet<Message> messages =
                MapDBStorage.getInstance().getMessages(id);
        final byte[] body = null == messages ?
                MAPPER.writeValueAsBytes(Collections.EMPTY_LIST) :
                    MAPPER.writeValueAsBytes(messages);
        final HttpResponse.Builder responseBuilder =
                new HttpResponse.Builder(200, "OK")
                .header(HttpHeaders.CONTENT_TYPE,
                        HttpConstants.APPLICATION_JSON_UTF_8)
                .header(HttpHeaders.CONTENT_LENGTH,
                        Integer.toString(body.length))
                .header(HttpHeaders.CONTENT_ENCODING,
                        HttpConstants.IDENTITY_ENCODING);
        final byte[] response = HttpResponse.serialize(responseBuilder.build());
        return new ByteBuffer[] {ByteBuffer.wrap(response),
                ByteBuffer.wrap(body)};
    }
}
