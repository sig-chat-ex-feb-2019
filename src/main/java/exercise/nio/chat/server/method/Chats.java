package exercise.nio.chat.server.method;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.net.HttpHeaders;

import exercise.nio.chat.server.Activity;
import exercise.nio.chat.server.annotations.Path;
import exercise.nio.chat.server.data.Chat;
import exercise.nio.chat.server.data.User;
import exercise.nio.chat.server.data.storage.MapDBStorage;
import exercise.nio.chat.server.http.CommonResponses;
import exercise.nio.chat.server.http.HttpConstants;
import exercise.nio.chat.server.http.HttpRequest;
import exercise.nio.chat.server.http.HttpResponse;

/**
 * Activity to handle adding and retrieving user chats
 */
@Path("/chats")
public class Chats implements Activity {
    private static final Pattern USER_ID_PATTERN =
            Pattern.compile(".*userId=([^&]+).*");
    private static final ObjectMapper MAPPER = new ObjectMapper();

    @Override
    public ByteBuffer[] invoke(HttpRequest request, Object... args)
            throws IOException {
        switch (request.getMethod()) {
        case "POST":
            return doChats(request);

        case "GET":
            return doGetUserChats(request);
        }
        return new ByteBuffer[] {ByteBuffer.wrap(CommonResponses.BAD_REQUEST)};
    }

    @Override
    public int getQueueingNiceness() {
        return Activity.NICENESS_LOW;
    }

    @Override
    public boolean shouldRetry() {
        return false;
    }

    /**
     * Create a new chat between two users.
     * The users must be mutually contacts.
     * @param request the HttpRequest
     * @return a ByteBuffer[] containing an HTTP response
     */
    protected ByteBuffer[] doChats(HttpRequest request) {
        final Chat newChat;
        try {
            newChat = MAPPER.readValue(request.getMessageBody(), Chat.class);
        } catch(IOException e) {
            return new ByteBuffer[]
                    {ByteBuffer.wrap(CommonResponses.BAD_REQUEST)};
        }
        if (2 != newChat.getParticipantIds().size()) {
            return new ByteBuffer[]
                    {ByteBuffer.wrap(CommonResponses.BAD_REQUEST)};
        }
        final Long[] userIds =
                newChat.getParticipantIds().toArray(new Long[] {});
        final MapDBStorage db = MapDBStorage.getInstance();
        final User userA = db.getUser(userIds[0]);
        final User userB = db.getUser(userIds[1]);
        if (null == userA || null == userB ||
                !userA.getContacts().contains(userB.getId()) ||
                !userB.getContacts().contains(userA.getId())) {
            return new ByteBuffer[]
                    {ByteBuffer.wrap(CommonResponses.FORBIDDEN)};
        }
        MapDBStorage.getInstance().persist(newChat);
        return new ByteBuffer[] {ByteBuffer.wrap(CommonResponses.OK)};
    }

    /**
     * Retrieve a user's chats
     * @param request the HttpRequest
     * @return a ByteBuffer[] containing an HTTP response
     * @throws IOException if there is an error serializing the response
     */
    protected ByteBuffer[] doGetUserChats(HttpRequest request)
            throws IOException {
        final Matcher userIdMatcher =
                USER_ID_PATTERN.matcher(request.getURI().getQuery());
        if (!userIdMatcher.matches()) {
            return new ByteBuffer[]
                    {ByteBuffer.wrap(CommonResponses.BAD_REQUEST)};
        }
        Long userId;
        try {
            userId = Long.parseLong(userIdMatcher.group(1));
        } catch(NumberFormatException e) {
            return new ByteBuffer[]
                    {ByteBuffer.wrap(CommonResponses.BAD_REQUEST)};
        }
        final User user = MapDBStorage.getInstance().getUser(userId);
        if (null == user) {
            return new ByteBuffer[]
                    {ByteBuffer.wrap(CommonResponses.NOT_FOUND)};
        }
        final List<Chat> userChats =
                user.getChatIds().parallelStream()
                .map((cid) -> MapDBStorage.getInstance().getChat(cid))
                .collect(Collectors.toList());
        final byte[] body = MAPPER.writeValueAsBytes(userChats);
        final HttpResponse.Builder responseBuilder =
                new HttpResponse.Builder(200, "OK")
                .header(HttpHeaders.CONTENT_TYPE,
                        HttpConstants.APPLICATION_JSON_UTF_8)
                .header(HttpHeaders.CONTENT_LENGTH,
                        Integer.toString(body.length))
                .header(HttpHeaders.CONTENT_ENCODING,
                        HttpConstants.IDENTITY_ENCODING);
        final byte[] response = HttpResponse.serialize(responseBuilder.build());
        return new ByteBuffer[] {ByteBuffer.wrap(response),
                ByteBuffer.wrap(body)};
    }
}
