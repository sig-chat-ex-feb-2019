package exercise.nio.chat.server;

import java.io.IOException;
import java.nio.ByteBuffer;

import exercise.nio.chat.server.http.HttpRequest;

/**
 * An interface for activities that respond to server requests
 */
public interface Activity {
    int NICENESS_LOW = -10;
    int NICENESS_MED_LOW = -5;
    int NICENESS_DEFAULT = 1;
    int NICENESS_MED_HIGH = 5;
    int NICENESS_HIGH = 10;

    /**
     * Invoke the activity and return the response.
     * @param request the request that the activity will process
     * @param args any arguments required for processing the request
     * @return a response as an array of ByteBuffer
     * @throws IOException if there is an input output error during processing
     */
    ByteBuffer[] invoke(HttpRequest request, Object... args)
            throws IOException;

    /**
     * A lower niceness indicates that an activity is preferred to run before
     * activities with a higher niceness
     * @return the activity niceness relative to other activities
     */
    int getQueueingNiceness();

    /**
     * The Activity may return true to request a retry
     * @return true iff the activity cannot now but may be run in the near term
     */
    boolean shouldRetry();
}
