package exercise.nio.chat.server.http.parse;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.concurrent.NotThreadSafe;

import com.google.common.net.HttpHeaders;

import exercise.nio.chat.server.http.HttpConstants;

/**
 * An incremental HTTP request parser
 */
@NotThreadSafe
public class HttpRequestParser {
    private static final Pattern REQUEST_START_PATTERN =
            Pattern.compile("(\\w+)\\s+([^\\s]+?)\\s+HTTP/(\\d+)\\.(\\d+)");
    private static final Pattern MESSAGE_HEADER_PATTERN =
            Pattern.compile("\\s*([^:]+?)\\s*:\\s*(.*?)\\s*");
    private final ByteBuffer buffer = ByteBuffer.allocate(1024);
    private final CharsetDecoder decoder = Charset.forName("UTF-8").newDecoder();
    private final SocketChannel channel;
    private final StringBuilder headersBuilder = new StringBuilder();
    private final EndHeaderDetector ehd = new EndHeaderDetector();
    private String method = null;
    private URI uri = null;
    private Integer majorVersion = null;
    private Integer minorVersion = null;
    private String headerString = null;
    private Map<String, String> headers = null;
    private byte[] messageBody = null;
    private Integer contentLength = null;
    private Integer messageBytesRead = 0;

    /**
     * Constructor.
     * @param channel from which to read the request
     */
    public HttpRequestParser(SocketChannel channel) {
        this.channel = channel;
    }

    /**
     * Perform an incremental read
     * @throws IOException if data cannot be read
     */
    public void readMore() throws IOException {
        int bytes = channel.read(buffer);
        if (-1 == bytes) {
            channel.close();
            return;
        }
        buffer.flip();
        if (!ehd.isEndHeaders()) {
            readHeaders();
        }
        if (!isReady()) {
            readBody();
        }
        buffer.compact();
    }

    /**
     * Helper method to incrementally read to the end of the request headers
     * @throws IOException if data cannot be read
     */
    private void readHeaders() throws IOException {
        final int initialPosition = buffer.position();
        final int initialRemaining = buffer.remaining();
        final byte[] tmp = new byte[initialRemaining];
        buffer.get(tmp, 0, buffer.remaining());
        final InputStream bais =
                new ByteArrayInputStream(tmp);
        final InputStreamReader reader = new InputStreamReader(bais, decoder);
        for (int i = 0; i < initialRemaining; i++) {
            if (ehd.isEndHeaders()) {
                buffer.position(initialPosition + i);
                break;
            }
            final int c = reader.read();
            ehd.alterState((char) c);
            headersBuilder.appendCodePoint(c);
        }
        if (ehd.isEndHeaders()) {
            parseHeaders();
        }
    }

    /**
     * Helper method to read the request body
     * TODO: support different content and transfer encodings
     */
    private void readBody() {
        if (null == contentLength) {
            final String contentLengthStr =
                    headers.get(HttpHeaders.CONTENT_LENGTH.toLowerCase());
            if (null == contentLengthStr) {
                return;
            }
            contentLength = Integer.parseInt(contentLengthStr);
            messageBody = new byte[contentLength];
        }
        final int initialRemaining = buffer.remaining();
        buffer.get(messageBody, messageBytesRead, initialRemaining);
        messageBytesRead += initialRemaining;
    }

    /**
     * @return true iff all known request data has been parsed
     */
    public boolean isReady() {
        return null != uri &&
                ((null == headers.get(HttpHeaders.CONTENT_LENGTH.toLowerCase()) &&
                null == headers.get(HttpHeaders.TRANSFER_ENCODING.toLowerCase())) ||
                messageBytesRead.equals(contentLength));
    }

    /**
     * @return the request URI
     */
    public URI getURI() {
        return uri;
    }

    /**
     * @return the request method
     */
    public String getMethod() {
        return method;
    }

    /**
     * @return the HTTP major version
     */
    public int getMajorVersion() {
        return majorVersion;
    }

    /**
     * @return the HTTP minor version
     */
    public int getMinorVersion() {
        return minorVersion;
    }

    /**
     * @return the headers as a string
     */
    public String getHeaderString() {
        return headerString; 
    }

    /**
     * @return the headers as a map
     */
    public Map<String, String> getHeaders() {
        return headers;
    }

    /**
     * @return the message body as a byte[]
     */
    public byte[] getMessageBody() {
        return messageBody;
    }

    /**
     * Helper method to parse the HTTP headers into a Map
     */
    private void parseHeaders() {
        headerString = headersBuilder.toString();
        final StringTokenizer token = new StringTokenizer(headerString,
                HttpConstants.CRLF);
        String line = token.nextToken();
        final Matcher startMatcher = REQUEST_START_PATTERN.matcher(line);
        if (!startMatcher.matches()) {
            // TODO add logging
            return;
        }
        uri = URI.create(startMatcher.group(2));
        method = startMatcher.group(1);
        majorVersion = Integer.parseInt(startMatcher.group(3));
        minorVersion = Integer.parseInt(startMatcher.group(4));
        final Map<String, String> headers = new HashMap<>(token.countTokens() << 2);
        while (token.hasMoreTokens()) {
            line = token.nextToken();
            final Matcher fieldMatcher = MESSAGE_HEADER_PATTERN.matcher(line);
            if (!fieldMatcher.matches()) {
                // TODO add logging
                continue;
            }
            headers.put(fieldMatcher.group(1).toLowerCase(),
                    fieldMatcher.group(2));
        }
        this.headers = Collections.unmodifiableMap(headers);
    }

    /**
     * To find the end of the headers section of the request, look for a null
     * line as specified in RFC 822, section 3.1:
     * https://www.ietf.org/rfc/rfc822.txt
     */
    private static class EndHeaderDetector {
        private boolean r1, n1, r2, n2;
        {r1 = n1 = r2 = n2 = false;}

        /**
         * @return true iff the end of the headers section is detected
         */
        public boolean isEndHeaders() {
            return r1 && n1 && r2 && n2;
        }

        /**
         * @param c the next request char as input to the state machine
         */
        public void alterState(char c) {
            if (isEndHeaders()) {
                return;
            }
            if (!(r1 || n1 || r2 || n2) && '\r' == c) {
                r1 = true;
            } else if (r1 && !(n1 || r2 || n2) && '\n' == c) {
                n1 = true;
            } else if (r1 && n1 && !(r2 || n2) && '\r' == c) {
                r2 = true;
            } else if (r1 && n1 && r2 && !n2 && '\n' == c) {
                n2 = true;
            } else {
                r1 = n1 = r2 = n2 = false;
            }
        }
    }
}
