package exercise.nio.chat.server.http;

/**
 * Commonly used HTTP constants.
 */
public final class HttpConstants {
    /**
     * Private constructor.
     */
    private HttpConstants() { }

    /**
     * Carriage return character followed by a line feed
     */
    public static final String CRLF = "\r\n";

    /**
     * The HTTP version 1.1. version string
     */
    public static final String HTTP_1_1 = "HTTP/1.1";

    /**
     * An encoding representing no encoding
     */
    public static final String IDENTITY_ENCODING = "identity";

    /**
     * The application/http content type for HTTP messages
     */
    public static final String APPLICATION_HTTP = "application/http";

    /**
     * The application/json content type for JSON messages
     */
    public static final String APPLICATION_JSON = "application/json";

    /**
     * The charset parameter for content-type
     */
    public static final String CHARSET_UTF_8 = "; charset=utf-8";

    /**
     * Content-type application/json with the UTF-8 charset
     */
    public static final String APPLICATION_JSON_UTF_8 =
            APPLICATION_JSON + CHARSET_UTF_8;
}
