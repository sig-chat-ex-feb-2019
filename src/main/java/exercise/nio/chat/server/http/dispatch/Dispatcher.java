package exercise.nio.chat.server.http.dispatch;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.NavigableSet;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

import javax.annotation.Priority;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.collections.impl.block.factory.Predicates;
import org.reflections.Reflections;

import exercise.nio.chat.server.Activity;
import exercise.nio.chat.server.annotations.Path;
import exercise.nio.chat.server.http.CommonResponses;
import exercise.nio.chat.server.http.HttpRequest;
import exercise.nio.chat.server.http.parse.HttpRequestParser;

/**
 * Dispatcher acts as the second pipe in a non-blocking io pipeline.
 * It has the following ordered tasks:
 * 1. Poll from queue of non-blocking SocketChannels that have open connections,
 *    registering newly connected channels with a selector on OP_READ.
 * 2. Select channels that have data to read and incrementally parse incoming
 *    requests.
 * 3. Match fully parsed requests using a routing table to activities that
 *    handle those requests, and then invoke the activities in priority order.
 */
public class Dispatcher implements Callable<Void> {
    private static final Logger logger = LogManager.getLogger(Dispatcher.class);
    /**
     * TODO: allow configuration for retry window
     */
    private static final int RETRY_WINDOW_MS = 10;
    private final BlockingQueue<SocketChannel> readyAcceptQ;
    private final BlockingQueue<ActivityInvocation> activityQ;
    private final BlockingQueue<ActivityInvocation> activityRetryQ;
    private final AtomicBoolean finished = new AtomicBoolean(false);
    private final Map<String, NavigableSet<Route>> handlers;
    private final Thread onShutdown = new Thread() {
        public void run() {
            finished.set(true);
        }
    };

    /**
     * Constructor.
     * @param readyAcceptQ the queue of incoming connected channels
     */
    public Dispatcher(BlockingQueue<SocketChannel> readyAcceptQ) {
        this.readyAcceptQ = readyAcceptQ;
        this.activityQ = new PriorityBlockingQueue<>(1000);
        this.activityRetryQ = new PriorityBlockingQueue<>(100);
        this.handlers = Collections.unmodifiableMap(discoverActivityHandlers());
        logger.info("loaded {} route handlers", handlers.size());
        Runtime.getRuntime().addShutdownHook(onShutdown);
    }

    @Override
    public Void call() throws IOException {
        final Selector readSelector = Selector.open();
        boolean blockOnPoll = false;
        while (!finished.get()) {
            try {
                pollChannels(readSelector, blockOnPoll);
            } catch (InterruptedException e) {
                logger.info("dispatcher thread interrupted, shutting down...");
                finished.set(true);
                Thread.interrupted();
            }
            selectChannels(readSelector);
            processActivities();
            blockOnPoll = readSelector.keys().isEmpty();
        }
        return null;
    }

    /**
     * Use reflection to discover any activity handlers with routing annotations
     * in the java package encompassing this project.
     * TODO: use an injection framework to discover activity handlers
     * @return the routing table keyed by path
     */
    private Map<String, NavigableSet<Route>> discoverActivityHandlers() {
        final Set<Class<?>> classes =
                new Reflections("exercise.nio.chat.server")
                .getTypesAnnotatedWith(Path.class);
        final Map<String, NavigableSet<Route>> handlers =
                new HashMap<>(classes.size() << 2);
        for (Class<?> c : classes) {
            final Path p = c.getAnnotation(Path.class);
            final Priority pr = c.getAnnotation(Priority.class);
            final Activity activity;
            try {
                 activity = Activity.class.cast(c.getConstructor().newInstance());
            } catch(IllegalAccessException | InstantiationException |
                    InvocationTargetException | NoSuchMethodException e) {
                logger.fatal("error loading activity for class {} and path {}",
                        c, p, e);
                throw new IllegalStateException(e);
            }
            final Route key;
            if (null == pr) {
                key = new Route(p.value(), activity);
            } else {
                key = new Route(pr.value(), p.value(), activity);
            }
            final NavigableSet<Route> routes;
            if (!handlers.containsKey(key.getKey())) {
                routes = new ConcurrentSkipListSet<>();
                handlers.put(key.getKey(), routes);
            } else {
                routes = handlers.get(key.getKey());
            }
            routes.add(key);
        }
        return handlers;
    }

    /**
     * Register incoming connected channels with the read selector.
     * @param readSelector a selector for new channels
     * @param block true to block once waiting for connections
     * @throws InterruptedException if the thread is interrupted while blocking
     */
    private void pollChannels(Selector readSelector, boolean block)
            throws InterruptedException {
        while (!readyAcceptQ.isEmpty() || block) {
            final SocketChannel channel = readyAcceptQ.poll(1000L,
                    TimeUnit.MILLISECONDS);
            if (null == channel) {
                break;
            }
            try {
                channel.configureBlocking(false);
                final SelectionKey key = channel.register(readSelector,
                        SelectionKey.OP_READ);
                key.attach(new HttpRequestParser(channel));
            } catch (IOException e) {
                try {
                    channel.close();
                } catch (IOException e2) {}
                logger.error("could not configure channel", e);
            }
            block = false;
        }
    }

    /**
     * Select channels with data to read and parse the data.
     * @param readSelector the selector for OP_READ
     * @throws IOException if there is an error during selection or read/write
     */
    private void selectChannels(Selector readSelector) throws IOException {
        if (0 == readSelector.selectNow()) {
            return;
        }
        final Iterator<SelectionKey> keys =
                readSelector.selectedKeys().iterator();
        while (keys.hasNext()) {
            final SelectionKey key = keys.next();
            final HttpRequestParser parser = (HttpRequestParser)
                    key.attachment();
            final SocketChannel channel = (SocketChannel) key.channel();
            try {
                parser.readMore();
            } catch (IOException e) {
                logger.error("io error reading from channel", e);
                try {
                    channel.close();
                } catch (IOException e2) { }
                key.cancel();
            }
            try {
                if (!parser.isReady()) {
                    continue;
                }
                final String path = parser.getURI().getPath().toLowerCase();
                final Route route = matchingRouteForPath(path);
                if (null == route) {
                    try {
                        logger.debug("couldn't find route for {}", path);
                        channel.write(ByteBuffer.wrap(CommonResponses.NOT_FOUND));
                        continue;
                    } finally {
                        channel.close();
                    }
                }
                final HttpRequest req = new HttpRequest(parser.getURI(),
                        parser.getMethod(), parser.getHeaderString(),
                        parser.getHeaders(), parser.getMessageBody());
                final ActivityInvocation ai =
                        new ActivityInvocation(route.getActivity(),
                                System.currentTimeMillis(), req,
                                route.getCapturedPathArgs(path), channel);
                activityQ.offer(ai);
            } finally {
                keys.remove();
            }
        }
    }

    /**
     * Invoke activities in priority order.
     */
    private void processActivities() {
        final Collection<ActivityInvocation> toRetry = new LinkedList<>();
        while (!activityQ.isEmpty()) {
            final ActivityInvocation ai = activityQ.poll();
            final boolean shouldRetry = ai.invoke(false);
            if (shouldRetry) {
                toRetry.add(ai);
            }
        }
        while(!activityRetryQ.isEmpty()) {
            final ActivityInvocation ai = activityRetryQ.poll();
            final boolean force = RETRY_WINDOW_MS <
                    (System.currentTimeMillis() - ai.getCreationTime());
            final boolean shouldRetry = ai.invoke(force);
            if (shouldRetry) {
                toRetry.add(ai);
            }
        }
        activityRetryQ.addAll(toRetry);
    }

    /**
     * Given a path, find the highest priority route that accepts the path
     * @param path the path to route
     * @return a matching Route or null is none is found
     */
    protected Route matchingRouteForPath(String path) {
        final Optional<NavigableSet<Route>> routes =
                path.chars().collect(() -> {
                    final ArrayList<StringBuilder> a = new ArrayList<>();
                    a.add(new StringBuilder());
                    return a;
                }, (a, ic) -> {
                    final char c = (char) ic;
                    if (0 != a.get(a.size() - 1).length() && c == '/') {
                        final StringBuilder next = new StringBuilder();
                        next.append(a.get(a.size() - 1));
                        next.append(c);
                        a.add(next);
                    } else {
                        a.get(a.size() - 1).append(c);
                    }
                }, (a, b) -> {
                    for (StringBuilder sb : b) {
                        sb.insert(0, a.get(a.size() - 1));
                        a.add(sb);
                    }
                }).stream()
                .map((sb) -> handlers.get(sb.toString()))
                .filter(Predicates.notNull())
                .collect(Collectors.reducing((a, b) -> {
                    a.addAll(b);
                    return a;
                }));
        if (routes.isEmpty()) {
            return null;
        }
        for (Route r : routes.get()) {
            if (r.accepts(path)) {
                return r;
            }
        }
        return null;
    }
}
