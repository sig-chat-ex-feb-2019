package exercise.nio.chat.server.http;

/**
 * Frequently referenced HTTP responses
 */
public final class CommonResponses {
    /**
     * 200 OK
     */
    public static final byte [] OK;
    static {
        final HttpResponse r = new HttpResponse(200, "OK");
        OK = HttpResponse.serialize(r);
    }

    /**
     * 400 Bad Request
     */
    public static final byte [] BAD_REQUEST;
    static {
        final HttpResponse r = new HttpResponse(400, "Bad Request");
        BAD_REQUEST = HttpResponse.serialize(r);
    }

    /**
     * 403 Forbidden
     */
    public static final byte [] FORBIDDEN;
    static {
        final HttpResponse r = new HttpResponse(403, "Forbidden");
        FORBIDDEN = HttpResponse.serialize(r);
    }

    /**
     * 404 Not Found
     */
    public static final byte [] NOT_FOUND;
    static {
        final HttpResponse r = new HttpResponse(404, "Not Found");
        NOT_FOUND = HttpResponse.serialize(r);
    }

    /**
     * 500 Server Error.
     */
    public static final byte [] SERVER_ERROR;
    static {
        final HttpResponse r = new HttpResponse(500, "Internal Server Error");
        SERVER_ERROR = HttpResponse.serialize(r);
    }

    /**
     * Private constructor.
     */
    private CommonResponses() { }
}
