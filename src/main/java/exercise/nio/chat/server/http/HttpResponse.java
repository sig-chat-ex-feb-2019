package exercise.nio.chat.server.http;

import java.nio.charset.Charset;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.annotation.concurrent.Immutable;

/**
 * POJO representing an HTTP response
 * TODO: if possible replace with java.net.http.HttpResponse
 * TODO: implement equals and hashCode
 */
@Immutable
public class HttpResponse {
    private static final Charset UTF_8 = Charset.forName("UTF-8");

    private final int status;
    private final String reason;
    private final Map<String, String> headers;

    /**
     * Constructor for a response with no headers
     * @param status the response status
     * @param reason the reason for the response
     */
    public HttpResponse(int status, String reason) {
        this(status, reason, Collections.emptyMap());
    }

    /**
     * Constructor.
     * @param status the response status
     * @param reason the reason for the response
     * @param headers the response headers as a map
     */
    public HttpResponse(int status, String reason, Map<String, String> headers) {
        this.status = status;
        this.reason = reason;
        this.headers = headers;
    }

    /**
     * @return the response status
     */
    public int getStatus() {
        return status;
    }

    /**
     * @return the reason for the response
     */
    public String getReason() {
        return reason;
    }

    /**
     * @return the response headers
     */
    public Map<String, String> getHeaders() {
        return headers;
    }

    /**
     * Serialize a UTF-8 encoded response into a byte array.
     * @param response the response to serialize
     * @return the serialized response
     */
    public static byte[] serialize(HttpResponse response) {
        final StringBuilder builder = new StringBuilder();
        builder.append(HttpConstants.HTTP_1_1);
        builder.append(" ");
        builder.append(response.getStatus());
        builder.append(" ");
        builder.append(response.getReason());
        builder.append(HttpConstants.CRLF);
        for (Map.Entry<String, String> e : response.getHeaders().entrySet()) {
            builder.append(e.getKey());
            builder.append(": ");
            builder.append(e.getValue());
            builder.append(HttpConstants.CRLF);
        }
        builder.append(HttpConstants.CRLF);
        return builder.toString().getBytes(UTF_8);
    }

    /**
     * A builder pattern for responses
     */
    public static class Builder {
        private final Integer status;
        private final String reason;
        private final Map<String, String> headers = new LinkedHashMap<>();

        /**
         * Constructor.
         * @param status the response status
         * @param reason the reason for the response
         */
        public Builder(Integer status, String reason) {
            this.status = status;
            this.reason = reason;
        }

        /**
         * Add a header
         * @param field the header field
         * @param value the header value
         * @return the Builder instance
         */
        public Builder header(String field, String value) {
            headers.put(field, value);
            return this;
        }

        /**
         * @return an HttpResponse with the current contents of the Builder
         */
        public HttpResponse build() {
            return new HttpResponse(status, reason,
                    Collections.unmodifiableMap(headers));
        }
    }
}
