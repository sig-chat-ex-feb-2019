package exercise.nio.chat.server.http.dispatch;

import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.concurrent.Immutable;

import exercise.nio.chat.server.annotations.Path;
import exercise.nio.chat.server.Activity;

/**
 * Pairs a route with an Activity, matches paths to the route, and extracts args
 * from the path using the routing information.
 * @see exercise.nio.chat.server.annotations.Path
 */
@Immutable
class Route implements Comparable<Route> {
    private static final Pattern ROUTE_PATTERN =
            Pattern.compile("(/?[^{]*?)(?:/(\\{?[^{/]+\\}?))");
    private final int priority;
    private final Pattern accept;
    private final String key;
    private final Activity activity;

    /**
     * Default priority constructor
     * @param route the route as extracted from Path annotations
     * @param activity the activity associated with the path
     */
    public Route(String route, Activity activity) {
        this(Path.Priority.DEFAULT, route, activity);
    }

    /**
     * Constructor
     * @param priority the route priority
     * @param route the route as extracted from Path annotations
     * @param activity the activity associated with the path
     */
    public Route(int priority, String route, Activity activity) {
        this.priority = priority;
        final Matcher m = ROUTE_PATTERN.matcher(route);
        if (!m.find()) {
            throw new IllegalArgumentException(route);
        }
        final StringBuilder kb = new StringBuilder(m.group(1));
        final StringBuilder pb = new StringBuilder(m.group(1));
        String next = m.group(2);
        boolean foundCaptureGroup = false;
        while (next != null) {
            if (next.startsWith("{") && next.endsWith("}")) {
                foundCaptureGroup = true;
                pb.append("/(?<");
                pb.append(next.subSequence(1, next.length() - 1));
                pb.append(">[^/]+)");
            } else {
                pb.append("/");
                pb.append(next.toLowerCase());
                if (!foundCaptureGroup) {
                    kb.append("/");
                    kb.append(next);
                }
            }
            next = m.find() ? m.group(2) : null;
        }
        this.accept = Pattern.compile(pb.toString());
        this.key = kb.toString().toLowerCase();
        this.activity = activity;
    }

    /**
     * @return key, the portion of the path used to find this route
     */
    public String getKey() {
        return key;
    }

    /**
     * @param path a URI path to check
     * @return true iff this route matches the given path
     */
    public boolean accepts(String path) {
        return accept.matcher(path).matches();
    }

    /**
     * @return the route priority
     */
    public int getPriority() {
        return priority;
    }

    /**
     * @return the activity associated with this route
     */
    public Activity getActivity() {
        return activity;
    }

    /**
     * @param path a URI path to check
     * @return elements captured from the path with the route derived pattern
     *         or null if the route doesn't match
     */
    public Object[] getCapturedPathArgs(String path) {
        final Matcher m = accept.matcher(path);
        if (!m.matches()) {
            return null;
        }

        final Object[] args = new Object[m.groupCount()];
        for (int i = 0; i < m.groupCount(); i++) {
            args[i] = m.group(i + 1);
        }

        return args;
    }

    @Override
    public int compareTo(Route other) {
        if (this == other) {
            return 0;
        }
        final int priorityOrder =
                Integer.valueOf(other.getPriority()).compareTo(priority);
        if (0 != priorityOrder) {
            return priorityOrder;
        }
        final int keyOrder = key.compareTo(other.key);
        if (0 != keyOrder) {
            return keyOrder;
        }
        return accept.toString().compareTo(other.accept.toString());
    }

    @Override
    public boolean equals(Object other) {
        if (null == other) {
            return false;
        }
        if (this == other) {
            return true;
        }
        if (other.getClass() != getClass()) {
            return false;
        }
        final Route hk = (Route) other;
        return Objects.equals(key, hk.key) &&
                Objects.equals(priority, hk.priority) &&
                Objects.equals(accept, hk.accept);
    }

    @Override
    public int hashCode() {
        return Objects.hash(key, priority, accept);
    }
}
