package exercise.nio.chat.server.http;

import java.net.URI;
import java.util.Map;

import javax.annotation.concurrent.Immutable;

/**
 * POJO representing an HttpRequest
 * TODO: if possible replace with java.net.http.HttpRequest
 * TODO: implement equals and hashCode
 */
@Immutable
public class HttpRequest {
    private final URI uri;
    private final String method;
    private final String headerString;
    private final Map<String, String> headers;
    private final byte[] messageBody;

    /**
     * Constructor
     * @param uri the request URI
     * @param method the request Method
     * @param headerString the headers as a string
     * @param headers the headers as a Map
     * @param messageBody the message body as a byte[]
     */
    public HttpRequest(URI uri, String method, String headerString,
            Map<String, String> headers, byte[] messageBody) {
        this.uri = uri;
        this.method = method;
        this.headerString = headerString;
        this.headers = headers;
        this.messageBody = messageBody;
    }

    /**
     * @return the URI
     */
    public URI getURI() {
        return uri;
    }

    /**
     * @return the request method
     */
    public String getMethod() {
        return method;
    }

    /**
     * @return the headers as a string
     */
    public String getHeaderString() {
        return headerString;
    }

    /**
     * @return the headers as a Map
     */
    public Map<String, String> getHeaders() {
        return headers;
    }

    /**
     * @return the message body as a byte[]
     */
    public byte[] getMessageBody() {
        return messageBody;
    }
}
