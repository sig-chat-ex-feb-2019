package exercise.nio.chat.server.http.dispatch;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

import javax.annotation.concurrent.Immutable;

import exercise.nio.chat.server.Activity;
import exercise.nio.chat.server.http.CommonResponses;
import exercise.nio.chat.server.http.HttpRequest;

/**
 * Encapsulates Activity invocation and the ordering thereof
 */
@Immutable
public class ActivityInvocation implements Comparable<ActivityInvocation> {
    /**
     * A time window to ensure that lower priority activities are not starved
     * TODO: allow configuration
     */
    private static final int TIME_WINDOW_MS = 10;
    private final Activity activity;
    private final long creationTime;
    private final HttpRequest request;
    private final Object[] args;
    private final SocketChannel outputChannel;

    /**
     * Constructor
     * @param activity the Activity that will be invoked
     * @param creationTime the time this invocation is created
     * @param request a request to give to the Activity
     * @param args for the Activity to process the request
     * @param outputChannel the channel that will receive the Activity output
     */
    public ActivityInvocation(Activity activity, long creationTime,
            HttpRequest request, Object[] args, SocketChannel outputChannel) {
        this.activity = activity;
        this.creationTime = creationTime;
        this.request = request;
        this.args = args;
        this.outputChannel = outputChannel;
    }

    /**
     * @return creation time
     */
    public long getCreationTime() {
        return creationTime;
    }

    /**
     * Invoke the Activity and output to the channel
     * @param force iff the response should be forced
     * @return true iff the invocation should be retried
     */
    public boolean invoke(boolean force) {
        ByteBuffer[] response;
        try {
            response = activity.invoke(request, args);
            if (!force && activity.shouldRetry()) {
                return true;
            }
        } catch (IOException e) {
            final byte[] error = CommonResponses.SERVER_ERROR;
            response = new ByteBuffer[] {ByteBuffer.wrap(error)};
        }
        try {
            outputChannel.write(response);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        } finally {
            try {
                outputChannel.close();
            } catch (IOException e) {}
        }
        return false;
    }

    /**
     * Activities that are created within TIME_WINDOW_MS are ordered ascendingly
     * from the lower value of getQueueingNiceness. If the nicenesses are equal,
     * Activities created within TIME_WINDOW_MS are ordered arbitrarily.
     * Activities created outside of TIME_WINDOW_MS of each other are ordered
     * ascendingly by least recent creation time so that more nice Activities
     * avoid starvation.
     */
    @Override
    public int compareTo(ActivityInvocation o) {
        if (this == o) {
            return 0;
        }
        if (TIME_WINDOW_MS < Math.abs(creationTime - o.creationTime)) {
            return Long.valueOf(o.creationTime).compareTo(creationTime);
        }
        final int qpThis = activity.getQueueingNiceness();
        final int qpOther = o.activity.getQueueingNiceness();
        final int queueingOrder = Integer.valueOf(qpThis).compareTo(qpOther);
        if (0 != queueingOrder) {
            return queueingOrder;
        }
        // if they have the same priority and are within the time window,
        // the order is arbitrary
        return Integer.valueOf(hashCode()).compareTo(o.hashCode());
    };
}
