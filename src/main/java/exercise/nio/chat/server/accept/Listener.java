package exercise.nio.chat.server.accept;

import java.io.IOException;
import java.net.SocketAddress;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Listener listens for incoming connections and places non blocking channels
 * into a queue for later processing in the non-blocking request pipeline.
 */
public class Listener implements Callable<Void> {
    private static final Logger logger = LogManager.getLogger(Listener.class);
    private final Queue<SocketChannel> readyAcceptQ;
    private final List<SocketAddress> addresses;
    private final CountDownLatch ready = new CountDownLatch(1);
    private final AtomicBoolean finished = new AtomicBoolean(false);
    private final Thread onShutdown = new Thread() {
        public void run() {
            finished.set(true);
        }
    };

    /**
     * Constructor.
     * @param readyAcceptQ the queue for incoming connected channels
     * @param addresses addresses to bind for listening
     */
    public Listener(Queue<SocketChannel> readyAcceptQ,
            List<SocketAddress> addresses) {
        this.readyAcceptQ = readyAcceptQ;
        this.addresses = addresses;
        Runtime.getRuntime().addShutdownHook(onShutdown);
    }

    /**
     * Wait for the listener to be ready to accept connections
     * @param timeout time to wait
     * @param unit time unit for wait time
     * @throws InterruptedException if the thread is interrupted while waiting
     */
    public void awaitReady(long timeout, TimeUnit unit)
            throws InterruptedException {
        ready.await(timeout, unit);
    }

    @Override
    public Void call() throws IOException {
        ServerSocketChannel acceptChannel = null;
        final Selector selector = Selector.open();
        try {
            acceptChannel = ServerSocketChannel.open();
            acceptChannel.configureBlocking(false);
            acceptChannel.register(selector, SelectionKey.OP_ACCEPT);
            for (SocketAddress address : addresses) {
                acceptChannel.bind(address);
            }
            ready.countDown();
            logger.info("Listening on {}", acceptChannel.getLocalAddress());
            while (!finished.get()) {
                selector.select();
                readyAcceptQ.offer(acceptChannel.accept());
            }
        } finally {
            if (null != acceptChannel) {
                acceptChannel.close();
            }
        }
        return null;
    }
}
