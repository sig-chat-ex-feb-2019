package exercise.nio.chat.server;

import java.io.IOException;
import java.io.InputStream;
import java.net.InetSocketAddress;
import java.nio.channels.SocketChannel;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import exercise.nio.chat.server.accept.Listener;
import exercise.nio.chat.server.data.User;
import exercise.nio.chat.server.data.storage.MapDBStorage;
import exercise.nio.chat.server.http.dispatch.Dispatcher;
import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;

/**
 * The main class and entry point for the chat server.
 */
public class Server {
    private static final Logger logger = LogManager.getLogger(Server.class);
    static final String TEST_ADDRESS = "localhost";
    static final int TEST_PORT = 3335;
    static final String USER_CONTACTS_FILE = "contacts.json";
    private static final ExecutorService ACCEPT =
            Executors.newFixedThreadPool(1);

    private final Listener listener;
    private final Dispatcher dispatcher;
    private final String address;
    private final int port;
    private final boolean shutdownDBBeforeExit;
    private final Thread onShutdown = new Thread() {
        public void run() {
            logger.debug("Server shutdown hook running...");
            ACCEPT.shutdown();
        }
    };

    /**
     * Constructor with TEST_ADDRESS and TEST_PORT.
     */
    public Server() {
        this(TEST_ADDRESS, TEST_PORT, false);
    }

    /**
     * Constructor.
     * @param address the address to bind
     * @param port the port to listen for connections
     * @param shutdownDBBeforeExit true to shutdown the db before exiting
     */
    public Server(String address, int port, boolean shutdownDBBeforeExit) {
        final BlockingQueue<SocketChannel> readyAcceptQ =
                new LinkedBlockingQueue<>();
        this.address = address;
        this.port = port;
        this.listener = new Listener(readyAcceptQ,
                Collections.singletonList(new InetSocketAddress(address,
                        port)));
        this.dispatcher = new Dispatcher(readyAcceptQ);
        this.shutdownDBBeforeExit = shutdownDBBeforeExit;
        Runtime.getRuntime().addShutdownHook(onShutdown);
    }

    /**
     * Get the server address
     * @return the server address
     */
    public String getAddress() {
        return address;
    }

    /**
     * Get the server port
     * @return the server port
     */
    public int getPort() {
        return port;
    }

    /**
     * Wait for the server to be ready to accept connections
     * @param timeout time to wait
     * @param unit time unit for wait time
     * @throws InterruptedException if the thread is interrupted while waiting
     */
    public void awaitReady(long timeout, TimeUnit unit)
            throws InterruptedException {
        listener.awaitReady(timeout, unit);
    }

    /**
     * Starting the server will load user contacts and start the Listener and
     * the Dispatcher. This call will block until interrupt or JVM shutdown.
     */
    public void start() {
        try {
            loadUserContacts();
        } catch (IOException e) {
            logger.error("Could not load user contact lists", e);
        }
        ACCEPT.submit(listener);
        try {
            dispatcher.call();
        } catch (IOException e) {
            logger.error("Dispatcher failed", e);
        } finally {
            ACCEPT.shutdown();
            try {
                ACCEPT.awaitTermination(1000L, TimeUnit.MILLISECONDS);
            } catch (InterruptedException | SecurityException e) {
                logger.error("Couldn't shutdown the listener", e);
            } finally {
                if (shutdownDBBeforeExit) {
                    MapDBStorage.getInstance().shutdown();
                }
            }
        }
    }

    /**
     * Load user contacts from class path resources and persist them in the DB.
     * TODO: allow users to authenticate and manage their own contacts.
     * @throws IOException if there is a problem reading the contacts
     */
    protected void loadUserContacts() throws IOException {
        // load user contacts and add them to the DB
        final ObjectMapper mapper = new ObjectMapper();
        final InputStream cs = ClassLoader.getSystemClassLoader()
                .getResourceAsStream(USER_CONTACTS_FILE);
        if (null == cs) {
            logger.fatal("cannot find user contacts resource {}, aborting...",
                    USER_CONTACTS_FILE);
            throw new IllegalStateException("missing user contacts data");
        }
        final Map<String, Set<Long>> contacts = mapper.readValue(cs,
                new TypeReference<Map<String, Set<Long>>> () {});
        int foundUsers = 0;
        for (Map.Entry<String, Set<Long>> e : contacts.entrySet()) {
            final long userId = Long.parseLong(e.getKey());
            final User user = new User(userId, e.getValue(),
                    Collections.emptySet());
            if (null == MapDBStorage.getInstance().getUser(userId)) {
                MapDBStorage.getInstance().persist(user);
            } else {
                foundUsers++;
            }
        }
        logger.info("loaded {} users from contacts, {} already exist",
                contacts.size(), foundUsers);
    }

    /**
     * For use with picocli to parse command line args from the main method
     */
    @Command(description = "Starts the chat server.", name = "simple-chat-server",
            mixinStandardHelpOptions = true, version = "Simple chat server 0.0.1")
    protected static class ServerOptions implements Callable<Server> {
        @Option(names = {"-a", "--address"}, paramLabel = "ADDRESS",
                description = "server address (default: ${DEFAULT-VALUE})")
        private String address = TEST_ADDRESS;
        @Option(names = {"-p", "--port"}, paramLabel = "PORT", required = true,
                description = "server port")
        private int port;
        @Option(names = {"-f", "--dbFile"}, paramLabel = "DB_FILE",
                description = "path to a file where the chat db is stored " +
                "(default: ${DEFAULT-VALUE})")
        private String dbFile = MapDBStorage.getDbFile();

        public String getAddress() {
            return address;
        }

        public int getPort() {
            return port;
        }

        public String getDbFile() {
            return dbFile;
        }

        @Override
        public Server call() {
            if (null != dbFile) {
                MapDBStorage.setDbFile(dbFile);
            }
            return new Server(address, port, true);
        }
    }

    /**
     * Main method to run the server as a command.
     * @see ServerOptions
     * @param args process args
     */
    public static void main(String[] args) {
        final Server server = CommandLine.call(new ServerOptions(), args);
        if (null != server) {
            server.start();
        }
    }
}
