package exercise.nio.chat.server.data;

import java.util.Objects;

import javax.annotation.concurrent.Immutable;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * POJO representing a chat message.
 */
@Immutable
public class Message implements Comparable<Message> {
    private final String id;
    private final Long timestamp;
    private final String message;
    private final long sourceUserId;
    private final long destinationUserId;
    private final int hashCode;

    /**
     * Constructor.
     * @param id the message id
     * @param timestamp message creation time
     * @param message the message
     * @param sourceUserId the source user id
     * @param destinationUserId the destination user id
     */
    @JsonCreator
    public Message(@JsonProperty("id") String id,
            @JsonProperty("timestamp") long timestamp,
            @JsonProperty("message") String message,
            @JsonProperty("sourceUserId") long sourceUserId,
            @JsonProperty("destinationUserId") long destinationUserId) {
        this.id = id;
        this.timestamp = timestamp;
        this.message = message;
        this.sourceUserId = sourceUserId;
        this.destinationUserId = destinationUserId;
        this.hashCode = Objects.hash(id, timestamp, message, sourceUserId,
                destinationUserId);
    }

    /**
     * @return the message id
     */
    public String getId() {
        return id;
    }

    /**
     * @return the message creation time
     */
    public long getTimestamp() {
        return timestamp;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @return the source user id
     */
    public long getSourceUserId() {
        return sourceUserId;
    }

    /**
     * @return the destination user id
     */
    public long getDestinationUserId() {
        return destinationUserId;
    }

    /**
     * Messages are ordered oldest to newest and arbitrarily if they are the
     * same age.
     */
    @Override
    public int compareTo(Message other) {
        if (null == other) {
            return -1;
        }
        if (this == other || id.equals(other.getId())) {
            return 0;
        }
        final int timeOrder = timestamp.compareTo(other.getTimestamp());
        if (0 != timeOrder) {
            return timeOrder;
        }
        return Integer.valueOf(hashCode).compareTo(other.hashCode);
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public boolean equals(Object other) {
        if (null == other) {
            return false;
        }
        if (this == other) {
            return true;
        }
        if (other.getClass() != getClass()) {
            return false;
        }
        final Message m = (Message) other;
        return Objects.equals(id, m.id) &&
                Objects.equals(timestamp, m.timestamp) &&
                Objects.equals(message, m.message) &&
                Objects.equals(sourceUserId, m.sourceUserId) &&
                Objects.equals(destinationUserId, m.destinationUserId);
    }
}
