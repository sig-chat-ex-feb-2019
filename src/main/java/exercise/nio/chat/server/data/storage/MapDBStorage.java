package exercise.nio.chat.server.data.storage;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.NavigableSet;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;

import org.mapdb.DB;
import org.mapdb.DBMaker;
import org.mapdb.DataInput2;
import org.mapdb.DataOutput2;
import org.mapdb.HTreeMap;
import org.mapdb.Serializer;
import org.mapdb.serializer.GroupSerializer;
import org.mapdb.serializer.GroupSerializerObjectArray;

import com.fasterxml.jackson.databind.ObjectMapper;

import exercise.nio.chat.server.data.Chat;
import exercise.nio.chat.server.data.Message;
import exercise.nio.chat.server.data.User;

/**
 * A storage layer for chat related objects based on MapDB:
 * @see <a href="http://www.mapdb.org">MapDB</a>
 * TODO: allow configuration
 */
public class MapDBStorage {
    public static final String TESTING_DB_FILE = "chats_testing.db";
    private static final ObjectMapper MAPPER = new ObjectMapper();
    private static final AtomicReference<MapDBStorage> INSTANCE =
            new AtomicReference<>();
    private static final AtomicReference<String> DB_FILE =
            new AtomicReference<>(TESTING_DB_FILE);
    private static final String DB_FILE_INIT_ERROR_MSG =
            "The db file name must be set before the db instance is created";
    private static final GroupSerializer<Chat> CHAT_SERIALIZER =
            jacksonSerializer(Chat.class);
    private static final GroupSerializer<User> USER_SERIALIZER =
            jacksonSerializer(User.class);
    private static final GroupSerializer<Message> MESSAGE_SERIALIZER =
            jacksonSerializer(Message.class);
    private final DB db;
    private final HTreeMap<Long, Chat> chats;
    private final HTreeMap<Long, User> users;
    private final Thread onShutdown = new Thread () {
        public void run() {
            shutdown();
        }
    };

    /**
     * Private constructor.
     */
    private MapDBStorage() {
        db = DBMaker.fileDB(DB_FILE.get())
                .fileMmapEnableIfSupported()
                .fileMmapPreclearDisable()
                .make();
        chats = db.hashMap("chats")
                .keySerializer(Serializer.LONG)
                .valueSerializer(CHAT_SERIALIZER)
                .createOrOpen();
        users = db.hashMap("users")
                .keySerializer(Serializer.LONG)
                .valueSerializer(USER_SERIALIZER)
                .createOrOpen();
        Runtime.getRuntime().addShutdownHook(onShutdown);
    }

    /**
     * @return the singleton instance of MapDBStorage
     */
    public static MapDBStorage getInstance() {
        if (null != INSTANCE.get()) {
            return INSTANCE.get();
        }
        INSTANCE.compareAndSet(null, new MapDBStorage());
        return INSTANCE.get();
    }

    /**
     * @return the db file
     */
    public static String getDbFile() {
        return DB_FILE.get();
    }

    /**
     * Sets the db file. Must be called prior to instance initialization.
     * @param file the db file to use
     * @throws IllegalArgumentException if the instance is already initialized
     */
    public static void setDbFile(String file) {
        if (null == file) {
            throw new IllegalArgumentException("db file cannot be null");
        }
        if (null != INSTANCE.get()) {
            throw new IllegalArgumentException(DB_FILE_INIT_ERROR_MSG);
        }
        DB_FILE.set(file);
    }

    /**
     * Store a chat
     * @param chat the chat to store
     */
    public void persist(Chat chat) {
        for (long id : chat.getParticipantIds()) {
            users.merge(id,
                    new User(id, Collections.emptySet(),
                            Collections.singleton(chat.getId())),
                    (ov, v) -> {
                        final Set<Long> chatIds =
                                new HashSet<>(v.getChatIds());
                        chatIds.addAll(ov.getChatIds());
                        return new User(id, ov.getContacts(), chatIds);
                    });
        }
        chats.put(chat.getId(), chat);
        db.commit();
    }

    /**
     * Retrieve a chat by id
     * @param id the chat id
     * @return the Chat or null if none exists
     */
    public Chat getChat(long id) {
        return chats.get(id);
    }

    /**
     * Store a chat message
     * @param chatId the chat id
     * @param message the message to store
     */
    public void persist(Long chatId, Message message) {
        final NavigableSet<Message> messages =
                db.treeSet("chat:" + chatId)
                .serializer(MESSAGE_SERIALIZER)
                .createOrOpen();
        messages.add(message);
        db.commit();
    }

    /**
     * Retrieve messages for a chat
     * @param chatId the chat id
     * @return an ordered set of messages of null if no messages exist
     */
    public NavigableSet<Message> getMessages(long chatId) {
        final String messagesDbName = "chat:" + chatId;
        if (!db.exists(messagesDbName)) {
            return null;
        }
        return Collections.unmodifiableNavigableSet(
                db.treeSet("chat:" + chatId)
                .serializer(MESSAGE_SERIALIZER)
                .open());
    }

    /**
     * Store a user
     * @param user the user to store
     */
    public void persist(User user) {
        users.put(user.getId(), user);
    }

    /**
     * Retrieve a user by id
     * @param id the user id
     * @return the user or null if none exists
     */
    public User getUser(long id) {
        return users.get(id);
    }

    /**
     * Shutdown the DB. This must be called on shutdown to preserve
     * data integrity.
     */
    public void shutdown() {
        db.close();
    }

    /**
     * Close the DB and delete all associated files.
     */
    public void destroy() {
        final Iterable<String> files = db.getStore().getAllFiles();
        db.close();
        for (String file : files) {
            new File(file).delete();
        }
    }

    /**
     * Create a MapDB GroupSerializer for a given type using Jackson
     * serialization.
     * @param clazz the type to serialize
     * @return the GroupSerializer with Jackson for underlying serialization
     */
    private static <T> GroupSerializer<T> jacksonSerializer(Class<T> clazz)  {
        return new GroupSerializerObjectArray<T> () {
            @Override
            public void serialize(DataOutput2 out, T value) throws IOException {
                final byte[] bytes = MAPPER.writeValueAsBytes(value);
                out.write(bytes);
            }

            @Override
            public T deserialize(DataInput2 input, int available)
                    throws IOException {
                return MAPPER.readValue(input, clazz);
            }

            @Override
            public void valueArraySerialize(DataOutput2 out, Object vals)
                    throws IOException {
                final byte[] bytes = MAPPER.writeValueAsBytes(vals);
                out.write(bytes);
            }

            @SuppressWarnings("unchecked")
            @Override
            public Object[] valueArrayDeserialize(DataInput2 in, int size)
                    throws IOException {
                final Class<T[]> arrayClazz;
                try {
                    arrayClazz =
                            (Class<T[]>)
                            Class.forName("[L" + clazz.getName() + ";");
                } catch(ClassNotFoundException e) {
                    throw new IllegalStateException(e);
                }
                return MAPPER.readValue(in, arrayClazz);
            }
        };
    }
}
