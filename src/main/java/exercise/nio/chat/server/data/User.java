package exercise.nio.chat.server.data;

import java.util.Objects;
import java.util.Set;

import javax.annotation.concurrent.Immutable;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * POJO representing a chat user.
 */
@Immutable
public class User {
    private final long id;
    private final Set<Long> contacts;
    private final Set<Long> chatIds;

    /**
     * Constructor.
     * @param id the user id
     * @param contacts the user's contacts as a set of user ids
     * @param chatIds the user's chats as chat ids
     */
    @JsonCreator
    public User(@JsonProperty("id") long id,
            @JsonProperty("contacts") Set<Long> contacts,
            @JsonProperty("chats") Set<Long> chatIds) {
        this.id = id;
        this.contacts = contacts;
        this.chatIds = chatIds;
    }

    /**
     * @return the user id
     */
    public long getId() {
        return id;
    }

    /**
     * @return the user's contacts as a set of user ids
     */
    public Set<Long> getContacts() {
        return contacts;
    }

    /**
     * @return the user's chat ids
     */
    public Set<Long> getChatIds() {
        return chatIds;
    }

    @Override
    public boolean equals(Object other) {
        if (null == other) {
            return false;
        }
        if (this == other) {
            return true;
        }
        if (other.getClass() != this.getClass()) {
            return false;
        }
        return Objects.equals(id, ((User) other).id) &&
                Objects.equals(contacts, ((User) other).contacts) &&
                Objects.equals(chatIds, ((User) other).chatIds);
    }
}
