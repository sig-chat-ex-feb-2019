package exercise.nio.chat.server.data;

import java.util.Collection;

import javax.annotation.concurrent.Immutable;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Objects;

/**
 * POJO representing a chat.
 */
@Immutable
public class Chat {
    private final long id;
    private final Collection<Long> participantIds;

    /**
     * Constructor.
     * @param id the chat id
     * @param participantIds the chat participant ids
     */
    @JsonCreator
    public Chat(@JsonProperty("id") long id,
            @JsonProperty("participantIds") Collection<Long> participantIds){
        this.id = id;
        this.participantIds = participantIds;
    }

    /**
     * @return the chat id
     */
    public long getId() {
        return id;
    }

    /**
     * @return the participant ids
     */
    public Collection<Long> getParticipantIds() {
        return participantIds;
    }

    @Override
    public boolean equals(Object other) {
        if (null == other) {
            return false;
        }
        if (this == other) {
            return true;
        }
        if (other.getClass() != getClass()) {
            return false;
        }
        return Objects.equal(id, ((Chat) other).id);
    }
}
