package exercise.nio.chat.server;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.junit.AfterClass;
import org.junit.Test;
import org.signal.entities.Chat;
import org.signal.entities.Generator;
import org.signal.entities.Message;
import org.signal.http.MessageSender;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import exercise.nio.chat.server.data.storage.MapDBStorage;

public class ServerIntegrationTest {
    private static final String INTEG_TEST_DB = "chats_integ_test.db";
    private static final int NUM_MESSAGES = 100;
    private static final ExecutorService SERVER =
            Executors.newFixedThreadPool(1);
    private static final Comparator<Message> MESSAGE_COMP =
            new Comparator<> () {
        @Override
        public int compare(Message o1, Message o2) {
            if (o1 == o2) {
                return 0;
            }
            final int timeOrder =
                    Long.valueOf(o1.getTimestamp())
                    .compareTo(o2.getTimestamp());
            if (0 == timeOrder) {
                return o1.getId().compareTo(o2.getId());
            }
            return timeOrder;
        }
    };
    private final Generator gen = new Generator("localhost", Server.TEST_PORT);
    private final MessageSender sender =
            new MessageSender("localhost", Server.TEST_PORT);

    @Test public void testWithGeneratedMessages()
            throws InterruptedException, IOException {
        MapDBStorage.setDbFile(INTEG_TEST_DB);
        final Server server = new Server();
        SERVER.execute(new Runnable() {
            public void run() {
                server.start();
            }
        });
        // wait for the server to start listening
        server.awaitReady(10L, TimeUnit.SECONDS);

        gen.loadUsers();
        final Map<String, Set<Long>> contacts = loadUsers();

        final Map<ChatKey, Chat> chats = new HashMap<>();
        final Map<Long, Set<Message>> messages =
                new HashMap<>(NUM_MESSAGES << 2);
        final Map<Long, Set<Long>> userChats = new HashMap<>(NUM_MESSAGES << 2);

        int messagesSent = 0;
        for (int i = 0; i < NUM_MESSAGES; i++) {
            final Message m = gen.generateMessage(false);
            if (!contacts.get(m.getSourceUserId().toString())
                    .contains(m.getDestinationUserId()) ||
                    !contacts.get(m.getDestinationUserId().toString())
                    .contains(m.getSourceUserId())) {
                continue;
            }
            final ChatKey k1 = new ChatKey(m.getSourceUserId(),
                    m.getDestinationUserId());
            final ChatKey k2 = new ChatKey(m.getDestinationUserId(),
                    m.getSourceUserId());
            final Chat c;
            if (chats.containsKey(k1) || chats.containsKey(k2)) {
                c = null == chats.get(k1) ? chats.get(k2) : chats.get(k1);
            } else {
                c = new Chat(m.getSourceUserId(), m.getDestinationUserId());
                assertFalse("new chat id must be unique",
                        messages.containsKey(c.getId()));
                messages.put(c.getId(), new TreeSet<>(MESSAGE_COMP));
                chats.put(k1, c);
                if (!userChats.containsKey(m.getSourceUserId())) {
                    userChats.put(m.getSourceUserId(), new HashSet<>());
                }
                if (!userChats.containsKey(m.getDestinationUserId())) {
                    userChats.put(m.getDestinationUserId(), new HashSet<>());
                }
                userChats.get(m.getSourceUserId()).add(c.getId());
                userChats.get(m.getDestinationUserId()).add(c.getId());
                sender.sendChat(c);
            }
            messages.get(c.getId()).add(m);
            sender.sendMessage(c.getId(), m);
            messagesSent++;
        }
        assertTrue("must send some valid messages", 0 < messagesSent);

        // the server currently takes about 10-15ms avg for writes
        // so wait sufficient time to process all requests
        Thread.sleep(1000);
        SERVER.shutdown();
        SERVER.awaitTermination(1000L, TimeUnit.MILLISECONDS);

        final MapDBStorage db = MapDBStorage.getInstance();
        for (Map.Entry<Long, Set<Message>> e : messages.entrySet()) {
            assertNotNull("db should persist chat: " + e.getKey(),
                    db.getChat(e.getKey()));
            assertNotNull("at least one message should exist: " + e.getKey(),
                    db.getMessages(e.getKey()));
            final List<String> dbIds =
                    db.getMessages(e.getKey()).stream()
                    .map((m) -> m.getId())
                    .collect(Collectors.toList());
            final List<String> sentIds = e.getValue().stream()
                    .map((m) -> m.getId().toString())
                    .collect(Collectors.toList());
            assertEquals("ids should match in order for " + e.getKey(),
                    sentIds, dbIds);
        }
        for (Map.Entry<Long, Set<Long>> e : userChats.entrySet()) {
            final Collection<Long> dbUserChats =
                    db.getUser(e.getKey()).getChatIds();
            assertNotNull("db should persist user chats", dbUserChats);
            assertEquals("db users chats should match requested chats",
                    e.getValue(), dbUserChats);
        }
    }

    private Map<String, Set<Long>> loadUsers() throws IOException {
        final ObjectMapper mapper = new ObjectMapper();
        final File cf = new File(ClassLoader.getSystemClassLoader()
                .getResource(Server.USER_CONTACTS_FILE).getFile());
        final Map<String, Set<Long>> contacts = mapper.readValue(cf,
                new TypeReference<Map<String, Set<Long>>> () {});
        assertNotNull("contacts map is required", contacts);
        assertFalse("contacts must contain entries", 0 == contacts.size());
        return contacts;
    }

    @AfterClass public static void cleanup() {
        MapDBStorage.getInstance().destroy();
    }

    private static class ChatKey {
        private final long source;
        private final long dest;

        public ChatKey(long source, long dest) {
            this.source = source;
            this.dest = dest;
        }

        @Override
        public int hashCode() {
            return Objects.hash(source, dest);
        }
    }
}
