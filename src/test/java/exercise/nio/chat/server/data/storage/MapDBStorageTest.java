package exercise.nio.chat.server.data.storage;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.NavigableSet;
import java.util.UUID;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import exercise.nio.chat.server.data.Chat;
import exercise.nio.chat.server.data.Message;
import exercise.nio.chat.server.data.User;

public class MapDBStorageTest {
    private static final String UNIT_TEST_DB = "chats_unit_test.db";

    @BeforeClass
    public static void setup() {
        MapDBStorage.setDbFile(UNIT_TEST_DB);
    }

    @Test public void testPersistChat() {
        final MapDBStorage db = MapDBStorage.getInstance();
        final Chat chat = new Chat(1L, Arrays.asList(1L, 2L));
        db.persist(chat);
        assertTrue("deserialized object should be the same",
                chat.equals(db.getChat(1)));
        assertTrue("chat should have correct participants",
                db.getChat(1L).getParticipantIds().equals(Arrays.asList(1L, 2L)));
        final User user1 = db.getUser(1L);
        final User user2 = db.getUser(2L);
        assertTrue("users should exist", null != user1 && null != user2);
        assertTrue("both users should be chat participants",
                user1.getChatIds().iterator().next() == 1L &&
                user2.getChatIds().iterator().next() == 1L);
    }

    @Test public void testPersistMessage() {
        final MapDBStorage db = MapDBStorage.getInstance();
        final Message m1 = new Message(UUID.randomUUID().toString(),
                10L, "m1", 1L, 2L);
        final Message m2 = new Message(UUID.randomUUID().toString(),
                18L, "m2", 2L, 1L);
        final Message m3 = new Message(UUID.randomUUID().toString(),
                15L, "m3", 1L, 2L);
        db.persist(1L, m1);
        db.persist(1L, m2);
        db.persist(1L, m3);
        final NavigableSet<Message> messages = db.getMessages(1L);
        assertNotNull("chat messages db should exist", messages);
        assertTrue("messages should be ordered correctly",
                Arrays.deepEquals(messages.toArray(),
                        new Object[] {m1, m3, m2}));
    }

    @AfterClass public static void cleanup() {
        MapDBStorage.getInstance().destroy();
    }
}
