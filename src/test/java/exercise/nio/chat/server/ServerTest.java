package exercise.nio.chat.server;

import static org.junit.Assert.assertEquals;

import java.util.Random;

import org.junit.Test;

import exercise.nio.chat.server.data.storage.MapDBStorage;
import picocli.CommandLine;
import picocli.CommandLine.MissingParameterException;

public class ServerTest {
    @Test public void testCliParsing() {
        final Server.ServerOptions opts = new Server.ServerOptions();
        final Server s = CommandLine.call(opts, new String[] {"-p", "3336"});
        assertEquals("opts address should be set to default",
                Server.TEST_ADDRESS, opts.getAddress());
        assertEquals("server address should be set to default",
                Server.TEST_ADDRESS, s.getAddress());
        assertEquals("opts port should be set to 3336", 3336, opts.getPort());
        assertEquals("server port should be set to 3336", 3336, s.getPort());
        assertEquals("opts dbFile should be set to default",
                MapDBStorage.TESTING_DB_FILE, opts.getDbFile());
        assertEquals("db file should be set to default",
                MapDBStorage.TESTING_DB_FILE, MapDBStorage.getDbFile());
    }

    @Test(expected = MissingParameterException.class) 
    public void testCliParsing_missingPort() {
        final Server.ServerOptions opts = new Server.ServerOptions();
        final CommandLine cmd = new CommandLine(opts);
        cmd.parse(new String[] {});
    }

    @Test public void testCliParsing_allPopulated() {
        final Server.ServerOptions opts = new Server.ServerOptions();
        final String address = "ahost";
        final Integer port = new Random().nextInt(65536);
        final String dbFile = "chats.db";
        final Server s = CommandLine.call(opts,
                new String[] {"-a", address, "-p", port.toString(),
                        "-f", dbFile});
        assertEquals("opts address should be set to " + address, address,
                opts.getAddress());
        assertEquals("server address should be set to " + address,
                address, s.getAddress());
        assertEquals("opts port should be set to " + port, (int) port,
                opts.getPort());
        assertEquals("server port should be set to " + port, (int) port,
                s.getPort());
        assertEquals("opts dbFile should be set to " + dbFile, dbFile,
                opts.getDbFile());
        assertEquals("db file should be set to " + dbFile,
                dbFile, MapDBStorage.getDbFile());
    }
}
