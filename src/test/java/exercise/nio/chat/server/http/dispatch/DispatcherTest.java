package exercise.nio.chat.server.http.dispatch;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.concurrent.ArrayBlockingQueue;

import org.junit.Test;

public class DispatcherTest {
    @Test public void testMatchingRouteForPath() {
        final Dispatcher d = new Dispatcher(new ArrayBlockingQueue<>(10));
        Route r = d.matchingRouteForPath("/foo/bar");
        assertNull("no matching route", r);

        r = d.matchingRouteForPath("/echo");
        assertNotNull("should find /echo route", r);

        r = d.matchingRouteForPath("/chats");
        assertNotNull("should find /chats route", r);

        r = d.matchingRouteForPath("/chats/1/messages");
        assertNotNull("should find /chats/{chatId}/messages route", r);
    }
}
